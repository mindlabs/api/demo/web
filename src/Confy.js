import { createApp } from "vue";
import { createRouter, createWebHashHistory } from "vue-router";
import MainView from "./MainView.vue";
import RoomView from "./RoomView.vue";

Object.keys(localStorage).forEach((conferenceId) => {
    try {
        let conference = JSON.parse(localStorage.getItem(conferenceId));
        if (Date.now() - conference.createdAt > 24 * 60 * 60 * 1000) {
            localStorage.removeItem(conferenceId);
        }
    } catch (error) {}
});

// Convert the old URI format (which uses `?`) to the new one (which uses `#/`)
if (location.href.match(/\?[^?#]+$/)) {
    location.assign(location.href.replace(/\?([^?#]+)$/, "#/$1"));
}

import { MindSDK, MindSDKOptions } from "mind-sdk-web";
import Application from "./Application.vue";
import "./Confy.css";

import PrimeVue from 'primevue/config';
import Lara from '@primevue/themes/lara';

let options = new MindSDKOptions();
MindSDK.initialize(options).then(() => {
    const router = createRouter({
        history: createWebHashHistory(),
        routes: [
            { path: "/:roomId([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})", component: RoomView },
            { path: "/:catchAll(.*)", component: MainView }
        ]
    });
    const application = createApp(Application);
    application.use(router);
    application.use(PrimeVue, {
        theme: {
            preset: Lara,
            options: {
                darkModeSelector: '.dark',
            }
        }
    });
    application.mount("#application");
});