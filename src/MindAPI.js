const APPLICATION_URL = window.location.protocol + "//" + window.location.hostname + "/00e1db72-c57d-478c-88be-3533d43c8b34";
const APPLICATION_TOKEN = "W1VJggwnvg2ldDdvSYES07tpLCWLDlD5nFakVJ6QSCPiZRpAMGyAzKW07OpM1IpceZ2WT5h5Mu7Ekt7WDTQzMUoQkVTRE4NdYUFE";
const HTTP_CONNECTION_TIMEOUT_SECONDS = 10;

export default class MindAPI {

    static createConference(name, layout) {
        return MindAPI.newHttpPost(APPLICATION_URL, { name: name, layout: layout, features: [ "vp9" ] }).then((responseDTO) => {
            let responseStatus = responseDTO.status;
            if (responseStatus === 200) {
                let conferenceDTO = responseDTO.data;
                return conferenceDTO.id; // FIXME: Why Mind Android/iOS SDKs return `APPLICATION_URI + "/" + conferenceDTO.id` instead?
            } else {
                throw new Error("Can't create a room: " + responseStatus);
            }
        });
    }

    static createParticipant(conferenceURI, name, layout, role) {
        return MindAPI.newHttpPost(conferenceURI + "/participants", { name: name, layout: layout, role: role }).then((responseDTO) => {
            let responseStatus = responseDTO.status;
            switch (responseStatus) {
                case 200:
                    let participantDTO = responseDTO.data;
                    return participantDTO.token;
                case 403:
                    return MindAPI.newHttpGet(conferenceURI + "/participants").then((responseDTO) => {
                        let responseStatus = responseDTO.status;
                        if (responseStatus === 200) {
                            let participantDTOs = responseDTO.data;
                            for (let participantDTO of participantDTOs) {
                                if (!participantDTO.online) {
                                    return participantDTO;
                                }
                            }
                            return null;
                        } else {
                            throw new Error("Can't get the list of participants: " + responseStatus);
                        }
                    }).then((offlineParticipant) => {
                        if (offlineParticipant != null) {
                            return MindAPI.newHttpDelete(conferenceURI + "/participants/" + offlineParticipant.id).then((responseDTO) => {
                                let responseStatus = responseDTO.status;
                                if (responseStatus === 204) {
                                    return MindAPI.createParticipant(conferenceURI, name, layout, role);
                                } else {
                                    throw new Error("Can't free space in the room: " + responseStatus);
                                }
                            });
                        } else {
                            throw new Error("The room is full");
                        }
                    });
                case 404:
                    throw new Error("The room doesn't exist");
                default:
                    throw new Error("Can't create a new participant: " + responseStatus);
            }
        });
    }

    static newHttpPost(uri, dto) {
        return MindAPI.newHttpRequest("POST", uri, dto);
    }

    static newHttpGet(uri) {
        return MindAPI.newHttpRequest("GET", uri);
    }

    static newHttpDelete(uri) {
        return MindAPI.newHttpRequest("DELETE", uri);
    }

    static newHttpRequest(method, url, dto) {
        return new Promise((resolve, reject) => {
            let expired = false;
            let controller = new AbortController();
            let timeoutId = setTimeout(() => {
                expired = true;
                controller.abort();
            }, HTTP_CONNECTION_TIMEOUT_SECONDS * 1000);
            fetch(url, {
                method: method,
                headers: {
                    "Content-Type": "application/json; charset=UTF-8",
                    "Accept": "application/json",
                    "Authorization": "Bearer " + APPLICATION_TOKEN,
                },
                body: dto ? JSON.stringify(dto) : undefined,
                signal: controller.signal
            }).then((response) => {
                let responseCode = response.status;
                let responseDTO = { status: responseCode };
                return response.text().then(text => {
                    let content = text.trim();
                    if (content.length > 0) {
                        switch (content.charAt(0)) {
                            case '[':
                                responseDTO.data = JSON.parse(content);
                                break;
                            case '{':
                                responseDTO.data = JSON.parse(content);
                                break;
                            default:
                                responseDTO.data = content;
                                break;
                        }
                    }
                    resolve(responseDTO);
                });
            }).catch((error) => {
                if (expired) {
                    reject(new Error("HTTP request timed out"));
                } else {
                    reject(error);
                }
            }).finally(() => {
                clearTimeout(timeoutId);
            });
        });
    }

}
