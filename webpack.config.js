const { VueLoaderPlugin } = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require('webpack');

module.exports = (env, argv) => {
    return {

        entry: "./src/Confy.js",

        output: {
            path: __dirname + "/dist/var/www/mind/api/demo",
            filename: "index.js",
        },

        devtool: "source-map",

        module: {
            rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: [ "babel-loader" ]
            }, {
                test: /\.vue$/,
                exclude: /node_modules/,
                use: [ "vue-loader" ]
            },{
                test: /\.css$/,
                use: [ "style-loader", "css-loader" ]
            },{
                test: /\.svg$/,
                exclude: /node_modules/,
                type: "asset/inline"
            }]
        },

        plugins: [
            new VueLoaderPlugin(),
            new HtmlWebpackPlugin({
                template: "./src/Confy.html"
            }),
            new webpack.DefinePlugin({ __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: true, __VUE_OPTIONS_API__: true, __VUE_PROD_DEVTOOLS__: false })
        ],

        stats: {
            modulesSpace: 1000
        }

    };
};
